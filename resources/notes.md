# Notes

## Description du projet

NIGHT a pour ambition de devenir une boite à outil multi-plateforme pour les MJ.

## Features

* Sauvegarde de personnages :
  * En fonction d'un moteur de jeu
  * Identité, caractéristique
  * Background et résumé, connaissances scénario, secret, ...
  * Possession, équipement
* Génération de personnages à la volé ou avec des contraintes
* Génération de villes/villages/biomes avec ses habitants avec des contraintes
* Génération d'événements
* Choix de l'environnement de jeu (Shaan, D&D, ...)
* Choix des règles de jeu
* Cartes
* Mode safe (pour les joueurs)
* Timeline de scénario
* Sauvegarde et historique de la partie

## Technos